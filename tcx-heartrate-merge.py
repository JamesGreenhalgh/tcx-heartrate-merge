#!/bin/env python3

import xml.etree.ElementTree as ET
import sys
import os

def usage():
    print("./tcx-heartrate-merge.py stravafile-with-correct-hr-data.tcx iFitfile-with-correct-distance-etc.tcx DEBUG[optional]")
    exit(1)

# Get the time and HR value from every Trackpoint in the XML
# and put it in a list
def getTimeAndHRListFromRootXML(myroot):
    trackies = []
    for trackpoint in myroot.iter(ns + 'Trackpoint'):
        if DEBUG: print(trackpoint.tag, trackpoint.attrib, trackpoint.text)
        time = trackpoint.find(ns + 'Time')
        hr = trackpoint.find(ns + 'HeartRateBpm')
        trackies.append([time.text,hr.find(ns + 'Value').text])
    return trackies

# Comparing the 2 lists of [Time,HR] points, pull the HR out of the first
# and put it in the second, based on Time text
def updateTimeAndHRList(StravaList, iFitList):
    i = 0
    while i < (len(StravaList) -1):
        # Get the indexes from the iFitList which fall between these 2 Strava Trackpoint items
        python_indices  = [index for (index, item) in enumerate(iFitList) if item[0] > StravaList[i][0] and item[0] < StravaList[i+1][0]]
        # Update the HR values at the right indexes
        for index in python_indices:
            iFitList[index][1] = StravaList[i][1]
        i = i + 1
    return iFitList

# Now, update the iFit root tree
def updateTheXMLs(myroot, TrackPoints):
    for trackpoint in myroot.iter(ns + 'Trackpoint'):
        for newDataPoint in TrackPoints:
            if trackpoint.find(ns + 'Time').text == newDataPoint[0]:
                if DEBUG: print(newDataPoint)
                # Find and update the heartrate
                heartRateElement = trackpoint.find(ns + 'HeartRateBpm')
                heartRateValue = heartRateElement.find(ns + 'Value')
                if DEBUG: print("Time: " + newDataPoint[0] + "Old HR: " + heartRateValue.text + " New HR: " + newDataPoint[1])
                heartRateValue.text = newDataPoint[1]
 
# Hierarchy in the file looks like this:
# Activities
## Activity
### Lap
#### Track
##### Trackpoint
###### HeartRateBpm
####### Value
###### Time

# We assume that we can rely on the times in the TCX files as they both use NTP (hopefully)
# We have more data in the iFit one than the strava one, so have to insert into the iFit one without leaving blanks
nsurl = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2"
ns = "{" + nsurl + "}"

# This is necessary because otherwise it inserts 'ns0' into all the tags and Strava doesn't like it
ET.register_namespace('', nsurl)

DEBUG = False
if len(sys.argv) < 3 or len(sys.argv) > 4:
    usage()
elif len(sys.argv) == 4:
    if str(sys.argv[3]) == "DEBUG":
        DEBUG = True

# Read the TCX XML files
StravaTree = ET.parse(str(sys.argv[1]))
iFitTree   = ET.parse(str(sys.argv[2]))

# Get the root element
StravaRoot = StravaTree.getroot()
iFitRoot   = iFitTree.getroot()

# Get the Time,HR data as a list from both root elements
StravaTimeandHRList  = getTimeAndHRListFromRootXML(StravaRoot)
iFitTimeandHRList    = getTimeAndHRListFromRootXML(iFitRoot)

# Put the Heartrate data from Strava list into the iFit list
updatedTimeAndHRList = updateTimeAndHRList(StravaTimeandHRList, iFitTimeandHRList)

# Update the actual iFit XML tree
updateTheXMLs(iFitRoot, updatedTimeAndHRList)
# Write the new XML tree out to a file
iFitFileName = os.path.splitext(str(sys.argv[2]))
iFitTree.write(iFitFileName[0] + "-UPDATED" + iFitFileName[1], xml_declaration=True)
